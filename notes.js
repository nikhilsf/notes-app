const fs = require('fs')
const chalk = require('chalk')
const log = console.log

const getNotes = () =>
{
    return "your notes..."
}


const readNote = (title) =>
{
    const notes = loadNotes()

    const duplicateNote = notes.find((note) => note.title === title)

    if(duplicateNote)
    {
        log('notes data ' + duplicateNote.title)
    }
    else
    {
        log('no note found')
    }
}

const addNote = (title, body) =>
{
    const notes = loadNotes()

    //const dupliacteNotes = notes.filter((note) => note.title === title)

    const dupliacteNote = notes.find((note) => note.title === title)

    if(!dupliacteNote)
    {
        notes.push({
            title : title,
            body : body
        })
        saveNotes(notes)
        log('New Note Added!!!')
    }
    else{
        log('note with title taken already')
    }
}


const removeNote = (title) =>
{
    const notes = loadNotes()
    
    //remove note if present
  
    const updatedNotes = notes.filter((note) => note.title !== title)
   
    if(notes.length === updatedNotes.length)
    {
        log('no note removed')
    }
    else{
        saveNotes(updatedNotes)
        log('note removed')
    }
    
}

const saveNotes = function(notes)
{
    jsonData = JSON.stringify(notes)
    fs.writeFileSync('notes.json',jsonData)
}

const loadNotes = () =>
{
    try
    {
    const dataBuffer = fs.readFileSync('notes.json')
    const jsonData = dataBuffer.toString()
    const jsonObjects = JSON.parse(jsonData)
    return jsonObjects
    }
    catch(e)
    {
        return []
    }
}


const listNotes = () =>
{
    const notes = loadNotes()
    log(chalk.green('your notes'))
    notes.forEach((note) => log(note.title))

}

module.exports = {
    getNotes : getNotes,
    addNote : addNote,
    removeNote : removeNote,
    listNotes : listNotes,
    readNote : readNote
}