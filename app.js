const chalk = require('chalk')
const notes = require('./notes.js')
const yargs = require('yargs')

const log = console.log

//create add command

yargs.command({
    command : 'add',
    describe : 'adding a new note',
    builder : {
        title : {
            describe : 'Note Title',
            demandOption : true,
            type : 'string'
        },
        body : {
            describe : 'Body Title',
            demandOption : true,
            type : 'string'
        }
    },
    handler : function(argv)
    {
        notes.addNote(argv.title,argv.body)
    }
})

//create remove command

yargs.command({
    command : 'remove',
    describe : 'removing a new note',
    builder : {
        title : {
            describe : 'remove title',
            demandOption : true,
            type : "string"
        }
    },
    handler : function(argv)
    {
        notes.removeNote(argv.title)
    }
})


//create list command

yargs.command({
    command : 'list',
    describe : 'listing the notes',
    handler : function()
    {
        log('listed the notes')
        notes.listNotes()
    }
})

//create read command

yargs.command({
    command : 'read',
    describe : 'reading a new note',
    builder : {
      title : {
          describe : "reading note title",
          demandOption : true,
          type : "string"
      }
    },
    handler : function(argv)
    {
        notes.readNote(argv.title)
    }
})

yargs.parse()